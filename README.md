jmasar-model is a library for data objects used in the jmasar-service. 

Java-based clients using the service can use this library to facilitate
marshalling/unmarshalling.

